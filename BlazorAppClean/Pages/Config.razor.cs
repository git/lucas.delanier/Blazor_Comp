﻿using BlazorAppClean.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace BlazorAppClean.Pages
{
    public partial class Config
    {
        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        public IOptions<PositionOptions> OptionsPositionOptions { get; set; }

        private PositionOptions positionOptions;

        protected override void OnInitialized()
        {
            base.OnInitialized();

            positionOptions = OptionsPositionOptions.Value;
        }
    }
}