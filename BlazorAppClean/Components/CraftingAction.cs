﻿using BlazorAppClean.Models;

namespace BlazorAppClean.Components
{
    public class CraftingAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}
