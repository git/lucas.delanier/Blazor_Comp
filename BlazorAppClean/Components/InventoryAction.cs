﻿using BlazorAppClean.Models;

namespace BlazorAppClean.Components
{
    public class InventoryAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}
