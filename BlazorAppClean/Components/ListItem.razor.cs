﻿using BlazorAppClean.Services;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.JSInterop;
using BlazorAppClean.Modals;
using BlazorAppClean.Models;
using BlazorAppClean.Services;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlazorAppClean.Components
{
    public partial class ListItem
    {

        public ListItem()
        {
            Actions = new ObservableCollection<ItemInInventory>();
            Actions.CollectionChanged += OnActionsCollectionChanged;
            this.ItemsInventory = new List<ItemInInventory> { };
        }

        public ObservableCollection<ItemInInventory> Actions { get; set; }
        public Item CurrentDragItem { get; set; }

        [Parameter]
        public List<Item> Items { get; set; }

        public List<ItemInInventory> ItemsInventory { get; set; }


        /// <summary>
        /// Gets or sets the java script runtime.
        /// </summary>
        [Inject]
        internal IJSRuntime JavaScriptRuntime { get; set; }

        [Inject]

        public IStringLocalizer<ListItem> Localizer { get; set; }


        private void OnActionsCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            _ = JavaScriptRuntime.InvokeVoidAsync("Crafting.AddActions", e.NewItems);
        }

        private List<Item>? items;

        private int totalItem;

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [CascadingParameter]
        public IModalService Modal { get; set; }
        public object Action { get; internal set; }

        protected override async Task OnInitializedAsync()
        {
            items = await DataService.getAll();
            totalItem = await DataService.Count();
            await base.OnInitializedAsync();
        }

    }
}