﻿using BlazorAppClean.Models;
using BlazorAppClean.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlazorAppClean.Components
{
    public partial class InventoryComponent
    {
        [Inject]
        public IDataService DataService { get; set; }

        private int totalItem;

        public Item CurrentDragItem { get; set; }

        [CascadingParameter]
        public InventoryComponent Parent { get; set; }
        public List<Item> RecipeItems { get; set; }
        public List<Item> Items { get; set; } = new List<Item>();
        public ObservableCollection<InventoryAction> Actions { get; set; }

        [Inject]
        internal IJSRuntime JavaScriptRuntime { get; set; }

        public InventoryComponent()
        {
            Actions = new ObservableCollection<InventoryAction>();
            Actions.CollectionChanged += OnActionsCollectionChanged;
            this.RecipeItems = new List<Item> { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null };
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.getAll();
            totalItem = await DataService.Count();

            StateHasChanged();
        }
        private void OnActionsCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            JavaScriptRuntime.InvokeVoidAsync("Crafting.AddActions", e.NewItems);
        }

    }
}
