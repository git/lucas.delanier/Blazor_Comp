﻿using BlazorAppClean.Models;

namespace BlazorAppClean.Components
{
    public class ItemInInventory
    {
        public int Index { get; set; }
        public Item? Item { get; set; }

    }
}